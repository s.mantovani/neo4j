from neo4j import GraphDatabase


class sospettati_neo4j:
    def _init__(self):
        self.driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j","password"))

    def close(self):
        if self.driver:
            self.driver.close()

    def read_query(self,query):
        session = self.driver.session()
        result = session.run(query)
        return result

    def home(self):
