import pyinputplus as ip
from neo4j import GraphDatabase
import traceback
from datetime import datetime

class neo4j:
    def __init__(self):
        self.driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "admin"))
        self.session = self.driver.session()     
        
   
    def menu(self):
        self.menu = ip.inputMenu(['Nome', 'Cella'], 
                                 prompt = 'Seleziona il metodo di ricerca: \n',
                                 numbered = True)
        if self.menu == 'Nome':
            self.result = self.__query("match (p:Person) return p.name")
            self.iterable_result = iter(self.result)
            self.name = ip.inputMenu([i['p.name'] for i in self.iterable_result], 
                                       prompt = 'Seleziona la persona: \n',
                                       numbered = True)
            self.result = self.__query(f"match (p:Person) -[:possiede]-> (Sim) -[c:connesso]->(:ripetitore) where p.name = '{self.name}' return c.data, c.ora")
            self.iterable_result = iter(self.result)
            self.time = []
            for i in self.iterable_result:
                self.time.append(str(i['c.data'])+' '+str(i['c.ora']))
            self.date = ip.inputMenu(self.time, 
                                       prompt = 'Seleziona la data: \n',
                                       numbered = True)
            self.result = self.__query(f"match (p:Person) -[:possiede]-> (Sim) -[c:connesso]->(r:ripetitore) where p.name = '{self.name}' and c.data = '{self.date.split(' ')[0]}' and c.ora ='{self.date.split(' ')[1]}' return r.nome")
            self.iterable_result = iter(self.result)
            for i in self.iterable_result:
                print(f"{self.name} è stato a {i['r.nome']} il {self.date.split(' ')[0]} alle {self.date.split(' ')[1]}")

        if self.menu == 'Cella':
            self.result = self.__query("match (r:ripetitore) return r.nome")
            self.iterable_result = iter(self.result)
            self.name = ip.inputMenu([i['r.nome'] for i in self.iterable_result], 
                                       prompt = 'Seleziona la cella: \n',
                                       numbered = True)
            try:
                self.result = self.__query(f"match (r:ripetitore) <-[c:connesso]- (s:Sim) where r.nome = '{self.name}' return c.data, c.ora")
                self.iterable_result = iter(self.result)
                self.time = []
                for i in self.iterable_result:
                    self.time.append(str(i['c.data'])+' '+str(i['c.ora']))
                self.date = ip.inputMenu(self.time, 
                                           prompt = 'Seleziona la data: \n',
                                           numbered = True)
            except:
                print('Nessuno si è collegato a questo ripetitore')
            else:
                self.result = self.__query(f"match (r:ripetitore) <-[c:connesso]- (s:Sim) <-[:possiede]- (p:Person) where r.nome = '{self.name}' and c.data = '{self.date.split(' ')[0]}' and c.ora = '{self.date.split(' ')[1]}' return p.name, s.numero")
                self.iterable_result = iter(self.result)
                for i in self.iterable_result:
                    print(f"{i['p.name']}, numero: {i['s.numero']}, è stato a {self.name} il {self.date.split(' ')[0]} alle {self.date.split(' ')[1]}")
        
    
    def __query(self, q):
        self.query = self.session.run(q)
        return self.query
          
        
        
        
        
if __name__ == '__main__':
    db = neo4j()
    db.menu()